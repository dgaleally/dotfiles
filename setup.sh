set -o vi

export CLICOLOR=1
export LSCOLORS=exfxcxdxbxegedabagacad

curl -o ~/.git-completion.bash https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
source ~/.git-completion.bash

curl -o ~/.git-prompt.sh https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
source ~/.git-prompt.sh

export PS1='\[\e[32m\u@\h\] \[\e[36m\w\] \[\e[33m\]\[\e[1m\] $(__git_ps1 " (%s)") \[\e[0m\] \n$ '



alias r='fc -e -'
alias salias='source ~/.bash_aliases'
alias sourceprofile='source ~/.bash_profile'
alias vialias='vi ~/.bash_aliases; salias'
alias vibash='vi ~/.bash_profile; sourceprofile'

